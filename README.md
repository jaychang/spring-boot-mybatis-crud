一：组件jar包说明：

	1.封装了数据实体对应的数据库列的一一映射关系
	2.提供了基本的增删改的sql生成
	3.提供了模糊匹配查询的sql生成
	4.提供了精确匹配查询的sql生成
	5.提供了基本的Mapper接口，里面有增删查改的基本方法
	6.提供了基本的抽象service，里面有增删查改的基本方法
	7：提供了分页的基本方法，使用com.github.pagehelper的pagehelper的 5.0.0的版本
二：组件jar包的的导入

	1.gradle 添加依赖 
          classpath "com.wolfking:mybatis-crud:1.0.0-SNAPSHOT"
	2.maven添加依赖
        <dependency>
        	<groupId>com.wolfking</groupId>
          	<artifactId>mybatis-crud</artifactId>
           <version>1.0.0-SNAPSHOT</version>
        </dependency>
三：jar包类的说明

	1.MyTable
		写在实体类上面，如下
    	@MyTable("sys_dict")
    	public class Dict extends DataEntity {}
    	sys_dict 是数据库对应的表名
	2.MyId
		写在主键属性上，如下，如果属性名和数据库的列名相同(严格区分大小写)，则不需要写括号里面的内容，
		主键的生成，第一版全部使用 UUID
		@MyId("id")
		protected String id;
	3.MyColumn
		写在主键属性上，如下，如果属性名和数据库的列名相同(严格区分大小写)，则不需要写括号里面的内容
		@MyColumn("remarks")
		protected String remarks; // 备注
	4.BaseMapper<T>  基本的mapper接口
	5.BaseSqlProvider  基本的sql 提供者
	6.BaseService<M extends BaseMapper<T>, T> 提供了基本的service，实体操作的service可以基于这个来写
四：mybatis的分页插件

	<plugin interceptor="com.github.pagehelper.PageInterceptor">
            <property name="helperDialect" value="mysql"/>
            <!-- 该参数默认为false -->
            <!-- 设置为true时，会将RowBounds第一个参数offset当成pageNum页码使用 -->
            <!-- 和startPage中的pageNum效果一样-->
            <property name="offsetAsPageNum" value="true"/>
            <!-- 该参数默认为false -->
            <!-- 设置为true时，使用RowBounds分页会进行count查询 -->
            <property name="rowBoundsWithCount" value="true"/>
            <!-- 设置为true时，如果pageSize=0或者RowBounds.limit = 0就会查询出全部的结果 -->
            <!-- （相当于没有执行分页查询，但是返回结果仍然是Page类型）-->
            <property name="pageSizeZero" value="true"/>
            <!-- 3.3.0版本可用 - 分页参数合理化，默认false禁用 -->
            <!-- 启用合理化时，如果pageNum<1会查询第一页，如果pageNum>pages会查询最后一页 -->
            <!-- 禁用合理化时，如果pageNum<1或pageNum>pages会返回空数据 -->
            <property name="reasonable" value="true"/>
            <!-- 3.5.0版本可用 - 为了支持startPage(Object params)方法 -->
            <!-- 增加了一个`params`参数来配置参数映射，用于从Map或ServletRequest中取值 -->
            <!-- 可以配置pageNum,pageSize,count,pageSizeZero,reasonable,orderBy,不配置映射的用默认值 -->
            <!-- 不理解该含义的前提下，不要随便复制该配置 -->
            <property name="params" value="pageNum=pageHelperStart;pageSize=pageHelperRows;"/>
            <!-- 支持通过Mapper接口参数来传递分页参数 -->
            <property name="supportMethodsArguments" value="false"/>
            <!-- always总是返回PageInfo类型,check检查返回类型是否为PageInfo,none返回Page -->
            <property name="returnPageInfo" value="none"/>
        </plugin>
五：jar包的使用方法

	1.pom.xml 和 build.gradle 添加依赖
	2.实体类的编写
		@MyTable("sys_dict")
		public class Dict{
               @MyId("id")
               private String id;
               @MyColumn
               private String description; // 描述
               @MyColumn("parent_id")
               private String parentId; // 父Id
		}
		
	2.mybatis的mapper的编写,继承BaseMapper<T>,T是实体类
	
		import  com.wolfking.mybatis.mapper.*;
		@Mapper   //这个是mybatis扫描的注解，根据mybatis-spring-boot-starter 版本来确定是否需要添加
		public interface DictMapper extends BaseMapper<Dict> {
		}
		
	3.在service层mybatis接口的使用,继承了抽象类的所有方法
		@Service
		public class DictService extends BaseService<DictMapper, Dict> {
			//其他业务代码
		}
六:根据ID查询，根据ID删除需要注意

	参数直接输入ID，不需要创建实体类
	service.getById("123");
	service.deleteById("123");
七:service方法讲解
	
	add：  添加实体
	update：更新实体
	deleteById：根据ID删除
	deleteEntity：根据ID删除
	getById： 根据ID查询
	getEntity：根据ID查询
	findAll：查询所有
	seleteVague：模糊匹配查询
	seleteAccuracy：精确匹配查询
	countAll：统计总数
	countVague：模糊匹配,查询总数
	countAccuracy：精确匹配,查询总数
	pageVague：模糊匹配分页查询
	pageAccuracy：精确匹配分页查询
	deleteAll: 删除所有实体
	deleteVague： 模糊匹配删除实体
	deleteAccuracy： 精确匹配删除实体
	
	
	
	
	
	
	
		
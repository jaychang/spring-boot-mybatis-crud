package com.wolfking.mybatis.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mybatis的表名的注解
 * <P>
 * @author   wolfking@赵伟伟
 * @mail     zww199009@163.com
 * @创作日期 2017年4月26日下午5:10:21
 * @版权     归wolfking所有
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyTable {
	String value() default "";
}
